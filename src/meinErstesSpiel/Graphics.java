package meinErstesSpiel;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import meinErstesSpiel.model.Model;

public class Graphics {

    private Model model;
    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    Image roteSchnecke = new Image("RoteSchnecke.png");
    Image blaueSchnecke = new Image("BlaueSchnecke.png");
    Image grüneSchnecke = new Image("GrüneSchnecke.png");
    Image gelbeSchnecke = new Image("GelbeSchnecke.png");
    Image pinkeSchnecke = new Image("PinkeSchnecke.png");
    Image gras = new Image("Gras.jpg");

    public void drawStartbildschirm() {

        if (model.isZielerreicht() == true) {
            gc.setFill(Color.GREEN);
            gc.drawImage(gras, 0, 0, 1500, 125);

            gc.setFill(Color.BURLYWOOD);
            gc.fillRect(0, 125, 1500, 500);

            gc.setFill(Color.GREEN);
            gc.drawImage(gras, 0, 575, 1500, 125);


            gc.setFill(Color.WHITE);
            gc.setFont(new Font("Times Roman", 50));
            gc.fillText("Die " + (model.getGewinner().getSchneckenname()) + " hat gewonnen!", 250, 200);
            gc.setFont(new Font(28));
            gc.fillText(model.spieler1.getName() + "      " + model.spieler1.getPunkte(), 250, 250);
            gc.fillText(model.spieler2.getName() + "      " + model.spieler2.getPunkte(), 250, 300);
            gc.fillText(model.spieler3.getName() + "      " + model.spieler3.getPunkte(), 250, 350);
            gc.fillText(model.spieler4.getName() + "      " + model.spieler4.getPunkte(), 250, 400);
            gc.fillText(model.spieler5.getName() + "      " + model.spieler5.getPunkte(),250, 450);
            gc.setFont(new Font(25));
            gc.fillText("Starte eine neue Runde mit der Enter-Taste.", 250,510);

        } else if (model.starteRennen() == true) {

            gc.setFill(Color.GREEN);
            gc.drawImage(gras, 0, 0, 1500, 125);

            gc.setFill(Color.BURLYWOOD);
            gc.fillRect(0, 125, 1500, 500);

            gc.setFill(Color.GREEN);
            gc.drawImage(gras, 0, 575, 1500, 125);

            gc.setFill(Color.DARKRED);
            gc.fillRect(100, 0, 10, 700);

            gc.setFill(Color.WHITE);
            gc.fillRect(1400, 0, 10, 700);

            gc.setFill(Color.WHITE);
            gc.setFont(new Font("Times Roman", 40));
            gc.fillText("Schneckenrennen 1.0  ", 600, 45);
            gc.fillText("Zeit  " + (model.getCounter() / 100), 30, 45);
            gc.setFont(new Font("Times Roman", 18));
            gc.fillText("Spieler:  " + (model.spieler1.getName()) + "   " + (model.spieler1.getSchneckenwette()) + "    " + (model.spieler1.getPunkte()), 200, 600);
            gc.fillText("Spieler:  " + (model.spieler2.getName()) + "   " + (model.spieler2.getSchneckenwette()) + "    " + (model.spieler2.getPunkte()), 200, 625);
            gc.fillText("Spieler:  " + (model.spieler3.getName()) + "   " + (model.spieler3.getSchneckenwette()) + "    " + (model.spieler3.getPunkte()), 200, 650);
            gc.fillText("Spieler:  " + (model.spieler4.getName()) + "   " + (model.spieler4.getSchneckenwette()) + "    " + (model.spieler4.getPunkte()), 200, 675);
            gc.fillText("Spieler:  " + (model.spieler5.getName()) + "   " + (model.spieler5.getSchneckenwette()) + "    " + (model.spieler5.getPunkte()),200,700);

            gc.setFill(Color.BLUE);
            gc.drawImage(blaueSchnecke, model.getSchnecke1().getPosX(), model.getSchnecke1().getPosY(), 65, 65);

            gc.setFill(Color.DARKGREEN);
            gc.drawImage(grüneSchnecke, model.getSchnecke2().getPosX(), model.getSchnecke2().getPosY(), 65, 65);

            gc.setFill(Color.RED);
            gc.drawImage(roteSchnecke, model.getSchnecke3().getPosX(), model.getSchnecke3().getPosY(), 65, 65);

            gc.setFill(Color.YELLOW);
            gc.drawImage(gelbeSchnecke, model.getSchnecke4().getPosX(), model.getSchnecke4().getPosY(), 65, 65);

            gc.setFill(Color.PINK);
            gc.drawImage(pinkeSchnecke, model.getSchnecke5().getPosX(),model.getSchnecke5().getPosY(), 65,65);

        } else {

            gc.setFill(Color.GREEN);
            gc.drawImage(gras, 0, 0, 1500, 125);

            gc.setFill(Color.BURLYWOOD);
            gc.fillRect(0, 125, 1500, 450);

            gc.setFill(Color.GREEN);
            gc.drawImage(gras, 0, 575, 1500, 125);

            gc.setFill(Color.WHITE);
            gc.setFont(new Font("Times Roman", 45));
            gc.fillText("Spieler", 240, 190);
            gc.setFont(new Font("Times Roman", 30));
            gc.fillText("1.   " + model.spieler1.getName(), 250, 250);
            gc.fillText(String.valueOf(model.spieler1.getPunkte()) + "   " + model.spieler1.getSchneckenwette(), 750, 250);
            gc.fillText("2.   " + model.spieler2.getName(), 250, 300);
            gc.fillText( String.valueOf(model.spieler2.getPunkte()) + "   " + model.spieler2.getSchneckenwette(), 750, 300);
            gc.fillText("3.   " + model.spieler3.getName(), 250, 350);
            gc.fillText(String.valueOf(model.spieler3.getPunkte()) + "   " + model.spieler3.getSchneckenwette(), 750, 350);
            gc.fillText("4.   " + model.spieler4.getName(), 250, 400);
            gc.fillText(String.valueOf(model.spieler4.getPunkte()) + "   " + model.spieler4.getSchneckenwette(), 750, 400);
            gc.fillText("5.   " + model.spieler5.getName(),250,450);
            gc.fillText(String.valueOf(model.spieler5.getPunkte()) + "   " + model.spieler5.getSchneckenwette(), 750,450);
            gc.setFont(new Font("Times Roman", 25));
            gc.fillText("Wähle deinen Spieler mit der Nummerntaste,",250,500);
            gc.fillText("dann wähle deine Schnecke: Blau (b), Grün (g), Rot (r), Gelb (y) oder Pink (p)",250,530);
            gc.fillText("Zum Starten drücke die Leertaste",250,560);



        }

    }
}
