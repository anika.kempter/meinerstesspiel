package meinErstesSpiel.model;

import java.util.Random;

public class Model {

    int counter = 0;
    int posX = 20;
    static final int posY = 150;
    boolean zielerreicht = false;
    boolean starteRennen = false;
    Schnecke gewinner;
    boolean phase1 = true;

    //Schnecken, welche am Rennen teilnehmen
    Schnecke schnecke1 = new Schnecke("Blaue Schnecke",posX,posY);
    Schnecke schnecke2 = new Schnecke("Grüne Schnecke", posX,posY +80);
    Schnecke schnecke3 = new Schnecke("Rote Schnecke", posX,posY +160);
    Schnecke schnecke4 = new Schnecke("Gelbe Schnecke",posX,posY +240);
    Schnecke schnecke5 = new Schnecke("Pinke Schnecke", posX, posY +320);



    public Schnecke getSchnecke1() {
        return schnecke1;
    }

    public Schnecke getSchnecke2() {
        return schnecke2;
    }

    public Schnecke getSchnecke3() {
        return schnecke3;
    }

    public Schnecke getSchnecke4() {
        return schnecke4;
    }

    public Schnecke getSchnecke5() {
        return schnecke5;
    }

    //Spieler, die mit Wetten
    public Spieler spieler1 = new Spieler("Schnecken-Wetten-Maxe", 50, null, 1);
    public Spieler spieler2 = new Spieler("Miriam Megawette", 50, null,2);
    public Spieler spieler3 = new Spieler("Tilly", 50, null,3);
    public Spieler spieler4 = new Spieler("Karla Krause", 50, null,4);
    public Spieler spieler5 = new Spieler("Svenja Wunderfitz", 50, null,5);


    //Getter


    public Spieler getSpieler1() {
        return spieler1;
    }

    public Spieler getSpieler2() {
        return spieler2;
    }

    public Spieler getSpieler3() {
        return spieler3;
    }

    public Spieler getSpieler4() {
        return spieler4;
    }

    public Spieler getSpieler5() {
        return spieler5;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setZielerreicht(boolean zielerreicht) {
        this.zielerreicht = zielerreicht;
    }

    public void setPhase1(boolean phase1) {
        this.phase1 = phase1;
    }

    public Schnecke getGewinner() {
        return gewinner;
    }

    public void setGewinner(Schnecke gewinner) {
        this.gewinner = gewinner;
    }

    public int getCounter() {
        return counter;
    }

    public boolean starteRennen() {
        return starteRennen;
    }

    public void setStarteRennen(boolean starteRennen) {
        this.starteRennen = starteRennen;
    }

    public boolean isZielerreicht() {
        return zielerreicht;
    }


    Random rand = new Random();




    public void update(long deltaMillis) {
            if (phase1 == true) {
                if (starteRennen == true) {
                    counter += deltaMillis;
                    int gesch1 = rand.nextInt(8) + 1;
                    schnecke1.bewegeSchnecke(+gesch1);
                    int gesch2 = rand.nextInt(8) + 1;
                    schnecke2.bewegeSchnecke(+gesch2);
                    int gesch3 = rand.nextInt(8) + 1;
                    schnecke3.bewegeSchnecke(+gesch3);
                    int gesch4 = rand.nextInt(8) + 1;
                    schnecke4.bewegeSchnecke(+gesch4);
                    int gesch5 = rand.nextInt(8) + 1;
                    schnecke5.bewegeSchnecke(+gesch5);

                    if (getSchnecke1().getPosX() >= 1400) {
                        starteRennen = false;
                        zielerreicht = true;
                        phase1 = false;
                        setGewinner(schnecke1);
                    } else if (getSchnecke2().getPosX() >= 1400) {
                        starteRennen = false;
                        zielerreicht = true;
                        phase1 = false;
                        setGewinner(schnecke2);
                    } else if (getSchnecke3().getPosX() >= 1400) {
                        starteRennen = false;
                        zielerreicht = true;
                        phase1 = false;
                        setGewinner(schnecke3);
                    } else if (getSchnecke4().getPosX() >= 1400) {
                        starteRennen = false;
                        zielerreicht = true;
                        phase1 = false;
                        setGewinner(schnecke4);
                    } else if (getSchnecke5().getPosX() >= 1400) {
                        starteRennen = false;
                        zielerreicht = true;
                        phase1 = false;
                        setGewinner(schnecke5);
                    }
                }

                counter += 0 * deltaMillis;
            }


            if (phase1 == false) {
                if (gewinner.schneckenname.equals(spieler1.schneckenwette)) {
                    spieler1.gewinnePunkte();
                    phase1 = true;
                } else if (spieler1.schneckenwette == null) {
                    spieler1.getPunkte();
                } else {
                    spieler1.verlierePunkte();
                    phase1 = true;
                }

                if (gewinner.schneckenname.equals(spieler2.schneckenwette)) {
                    spieler2.gewinnePunkte();
                    phase1 = true;
                } else if (spieler2.schneckenwette == null) {
                    spieler2.getPunkte();
                } else {
                    spieler2.verlierePunkte();
                    phase1 = true;
                }

                if (gewinner.schneckenname.equals(spieler3.schneckenwette)) {
                    spieler3.gewinnePunkte();
                    phase1 = true;
                } else if (spieler3.schneckenwette == null) {
                    spieler3.getPunkte();
                } else {
                    spieler3.verlierePunkte();
                    phase1 = true;
                }

                if (gewinner.schneckenname.equals(spieler4.schneckenwette)) {
                    spieler4.gewinnePunkte();
                    phase1 = true;
                } else if (spieler4.schneckenwette == null) {
                    spieler4.getPunkte();
                } else {
                    spieler4.verlierePunkte();
                    phase1 = true;
                }

                if (gewinner.schneckenname.equals(spieler5.schneckenwette)) {
                    spieler5.gewinnePunkte();
                    phase1 = true;
                } else if (spieler5.schneckenwette == null) {
                    spieler5.getPunkte();
                } else {
                    spieler5.verlierePunkte();
                    phase1 = true;
                }
            }
    }


    public void neueRunde() {
        setGewinner(null);
        resetCounter();
        setPosX(20);
        setZielerreicht(false);
        setStarteRennen(false);
        setPhase1(true);
        getSpieler1().setSchneckenwette(null);
        getSpieler2().setSchneckenwette(null);
        getSpieler3().setSchneckenwette(null);
        getSpieler4().setSchneckenwette(null);
        getSpieler5().setSchneckenwette(null);
        getSchnecke1().posX = 20;
        getSchnecke2().posX = 20;
        getSchnecke3().posX = 20;
        getSchnecke4().posX = 20;
        getSchnecke5().posX = 20;
    }


    public void resetCounter() {
        counter = 0;
    }
}
