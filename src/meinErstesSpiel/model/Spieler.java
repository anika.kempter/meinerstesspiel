package meinErstesSpiel.model;

public class Spieler {

    int punkte;
    String name;
    String schneckenwette;
    int spielernummer;

    public Spieler(String name, int punkte, String schneckenwette, int spielernummer) {
        this.name = name;
        this.punkte = punkte;
        this.schneckenwette = null;
        this.spielernummer = spielernummer;
    }


    public void verlierePunkte() {
        this.punkte -= 10;

    }

    public void gewinnePunkte() {
        this.punkte += 10;
    }

    public int getPunkte() {
        return punkte;
    }

    public void setPunkte(int punkte) {
        this.punkte = punkte;
    }

    public String getName() {
        return name;
    }

    public String getSchneckenwette() {
        return schneckenwette;
    }

    public void setSchneckenwette(String schneckenwette) {
        this.schneckenwette = schneckenwette;
    }

    public int getSpielernummer() {
        return spielernummer;
    }

    public void setSpielernummer(int spielernummer) {
        this.spielernummer = spielernummer;
    }
}
