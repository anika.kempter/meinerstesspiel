package meinErstesSpiel.model;

public class Schnecke{

     int posX;
     int posY;
     String schneckenname;

    public Schnecke(String schneckenname, int posX, int posY) {
        this.schneckenname = schneckenname;
        this.posX = posX;
        this.posY = posY;
    }

    public void bewegeSchnecke(Integer geschwindigkeit) {
        this.posX += geschwindigkeit;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public String getSchneckenname() {return schneckenname;}
}
