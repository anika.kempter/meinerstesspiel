package meinErstesSpiel;

import javafx.animation.AnimationTimer;
import meinErstesSpiel.model.Model;

public class Timer extends AnimationTimer {

    private Model model;
    private Graphics graphics;

    long lastMillis = -1;

    public Timer(Model model, Graphics graphics) {
        this.model = model;
        this.graphics = graphics;
    }


    @Override
    public void handle(long now) {
        //Updaten des Models
        long milllis = now / 1000000;

        long deltaMillis = 0;
        if (lastMillis != -1) {
            deltaMillis = milllis - lastMillis;
        }

        this.model.update(deltaMillis);
        lastMillis = milllis;

        //Malen der Graphiken


        graphics.drawStartbildschirm();
    }
}

