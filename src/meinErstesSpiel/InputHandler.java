package meinErstesSpiel;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import meinErstesSpiel.model.Model;



public class InputHandler {

    private Model model;


    public InputHandler(Model model) {
        this.model = model;
    }


    public void onKeyPressed(KeyEvent event) {
        //Starte Rennen mit Space
        if (event.getCode() == KeyCode.SPACE) {
            model.setStarteRennen(true);
        }


        if(event.getCode() == KeyCode.DIGIT1) {
            model.getSpieler1().setSpielernummer(10);
        } else if(event.getCode() == KeyCode.DIGIT2) {
            model.getSpieler2().setSpielernummer(20);
        } else if(event.getCode() == KeyCode.DIGIT3) {
            model.getSpieler3().setSpielernummer(30);
        } else if(event.getCode() == KeyCode.DIGIT4) {
            model.getSpieler4().setSpielernummer(40);
        } else if(event.getCode() == KeyCode.DIGIT5) {
            model.getSpieler5().setSpielernummer(50);
        }

        if(event.getCode() == KeyCode.B && model.getSpieler1().getSpielernummer() == 10) {
            model.getSpieler1().setSchneckenwette("Blaue Schnecke");
            model.getSpieler1().setSpielernummer(1);
        } else if (event.getCode() == KeyCode.R && model.getSpieler1().getSpielernummer() == 10){
            model.getSpieler1().setSchneckenwette("Rote Schnecke");
            model.getSpieler1().setSpielernummer(1);
        } else if (event.getCode() == KeyCode.Y && model.getSpieler1().getSpielernummer() == 10){
            model.getSpieler1().setSchneckenwette("Gelbe Schnecke");
            model.getSpieler1().setSpielernummer(1);
        } else if (event.getCode() == KeyCode.G && model.getSpieler1().getSpielernummer() == 10){
            model.getSpieler1().setSchneckenwette("Grüne Schnecke");
            model.getSpieler1().setSpielernummer(1);
        } else if (event.getCode() == KeyCode.P && model.getSpieler1().getSpielernummer() == 10) {
            model.getSpieler1().setSchneckenwette("Pinke Schnecke");
            model.getSpieler1().setSpielernummer(1);
        }

        if(event.getCode() == KeyCode.B && model.getSpieler2().getSpielernummer() == 20) {
            model.getSpieler2().setSchneckenwette("Blaue Schnecke");
            model.getSpieler2().setSpielernummer(2);
        } else if (event.getCode() == KeyCode.R && model.getSpieler2().getSpielernummer() == 20){
            model.getSpieler2().setSchneckenwette("Rote Schnecke");
            model.getSpieler2().setSpielernummer(2);
        } else if (event.getCode() == KeyCode.Y && model.getSpieler2().getSpielernummer() == 20){
            model.getSpieler2().setSchneckenwette("Gelbe Schnecke");
            model.getSpieler2().setSpielernummer(2);
        } else if (event.getCode() == KeyCode.G && model.getSpieler2().getSpielernummer() == 20){
            model.getSpieler2().setSchneckenwette("Grüne Schnecke");
            model.getSpieler2().setSpielernummer(2);
        } else if (event.getCode() == KeyCode.P && model.getSpieler2().getSpielernummer() == 20) {
            model.getSpieler2().setSchneckenwette("Pinke Schnecke");
            model.getSpieler2().setSpielernummer(2);
        }

        if(event.getCode() == KeyCode.B && model.getSpieler3().getSpielernummer() == 30) {
            model.getSpieler3().setSchneckenwette("Blaue Schnecke");
            model.getSpieler3().setSpielernummer(3);
        } else if (event.getCode() == KeyCode.R && model.getSpieler3().getSpielernummer() == 30){
            model.getSpieler3().setSchneckenwette("Rote Schnecke");
            model.getSpieler3().setSpielernummer(3);
        } else if (event.getCode() == KeyCode.Y && model.getSpieler3().getSpielernummer() == 30){
            model.getSpieler3().setSchneckenwette("Gelbe Schnecke");
            model.getSpieler3().setSpielernummer(3);
        } else if (event.getCode() == KeyCode.G && model.getSpieler3().getSpielernummer() == 30){
            model.getSpieler3().setSchneckenwette("Grüne Schnecke");
            model.getSpieler3().setSpielernummer(3);
        } else if (event.getCode() == KeyCode.P && model.getSpieler3().getSpielernummer() == 30) {
            model.getSpieler3().setSchneckenwette("Pinke Schnecke");
            model.getSpieler3().setSpielernummer(3);
        }

        if(event.getCode() == KeyCode.B && model.getSpieler4().getSpielernummer() == 40) {
            model.getSpieler4().setSchneckenwette("Blaue Schnecke");
            model.getSpieler4().setSpielernummer(4);
        } else if (event.getCode() == KeyCode.R && model.getSpieler4().getSpielernummer() == 40){
            model.getSpieler4().setSchneckenwette("Rote Schnecke");
            model.getSpieler4().setSpielernummer(4);
        } else if (event.getCode() == KeyCode.Y && model.getSpieler4().getSpielernummer() == 40){
            model.getSpieler4().setSchneckenwette("Gelbe Schnecke");
            model.getSpieler4().setSpielernummer(4);
        } else if (event.getCode() == KeyCode.G && model.getSpieler4().getSpielernummer() == 40){
            model.getSpieler4().setSchneckenwette("Grüne Schnecke");
            model.getSpieler4().setSpielernummer(4);
        } else if (event.getCode() == KeyCode.P && model.getSpieler4().getSpielernummer() == 40) {
            model.getSpieler4().setSchneckenwette("Pinke Schnecke");
            model.getSpieler4().setSpielernummer(4);
        }

        if(event.getCode() == KeyCode.B && model.getSpieler5().getSpielernummer() == 50) {
            model.getSpieler5().setSchneckenwette("Blaue Schnecke");
            model.getSpieler5().setSpielernummer(5);
        } else if (event.getCode() == KeyCode.R && model.getSpieler5().getSpielernummer() == 50){
            model.getSpieler5().setSchneckenwette("Rote Schnecke");
            model.getSpieler5().setSpielernummer(5);
        } else if (event.getCode() == KeyCode.Y && model.getSpieler5().getSpielernummer() == 50){
            model.getSpieler5().setSchneckenwette("Gelbe Schnecke");
            model.getSpieler5().setSpielernummer(5);
        } else if (event.getCode() == KeyCode.G && model.getSpieler5().getSpielernummer() == 50){
            model.getSpieler5().setSchneckenwette("Grüne Schnecke");
            model.getSpieler5().setSpielernummer(5);
        } else if (event.getCode() == KeyCode.P && model.getSpieler5().getSpielernummer() == 50) {
            model.getSpieler5().setSchneckenwette("Pinke Schnecke");
            model.getSpieler5().setSpielernummer(5);
        }

        if(event.getCode() == KeyCode.ENTER) {
            model.neueRunde();
        }

    }
}